--
-- PostgreSQL database dump
--

-- Dumped from database version 12.8
-- Dumped by pg_dump version 12.4

-- Started on 2021-08-17 20:58:53 +03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

-- TOC entry 209 (class 1259 OID 21402)
-- Name: appointment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appointment (
    id integer NOT NULL,
    "time" timestamp with time zone NOT NULL,
    duration integer NOT NULL,
    isactive boolean NOT NULL,
    specialityid integer,
    doctorid integer,
    patientid integer
);


ALTER TABLE public.appointment OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 21400)
-- Name: appointment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.appointment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.appointment_id_seq OWNER TO postgres;

--
-- TOC entry 3267 (class 0 OID 0)
-- Dependencies: 208
-- Name: appointment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.appointment_id_seq OWNED BY public.appointment.id;


--
-- TOC entry 205 (class 1259 OID 21352)
-- Name: doctor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.doctor (
    id integer NOT NULL,
    userid text NOT NULL,
    firstname text NOT NULL,
    lastname text NOT NULL,
    picture text,
    statement text DEFAULT ''::text,
    specialityid integer
);


ALTER TABLE public.doctor OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 21350)
-- Name: doctor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.doctor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.doctor_id_seq OWNER TO postgres;

--
-- TOC entry 3268 (class 0 OID 0)
-- Dependencies: 204
-- Name: doctor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.doctor_id_seq OWNED BY public.doctor.id;


--
-- TOC entry 207 (class 1259 OID 21380)
-- Name: patient; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.patient (
    id integer NOT NULL,
    userid text NOT NULL,
    firstname text NOT NULL,
    lastname text NOT NULL
);


ALTER TABLE public.patient OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 21378)
-- Name: patient_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.patient_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.patient_id_seq OWNER TO postgres;

--
-- TOC entry 3269 (class 0 OID 0)
-- Dependencies: 206
-- Name: patient_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.patient_id_seq OWNED BY public.patient.id;


--
-- TOC entry 211 (class 1259 OID 21436)
-- Name: shift; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shift (
    id integer NOT NULL,
    userid text NOT NULL,
    start timestamp with time zone NOT NULL,
    "end" timestamp with time zone NOT NULL,
    doctorid integer
);


ALTER TABLE public.shift OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 21434)
-- Name: shift_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.shift_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shift_id_seq OWNER TO postgres;

--
-- TOC entry 3270 (class 0 OID 0)
-- Dependencies: 210
-- Name: shift_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.shift_id_seq OWNED BY public.shift.id;


--
-- TOC entry 203 (class 1259 OID 21329)
-- Name: speciality; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.speciality (
    id integer NOT NULL,
    name text NOT NULL,
    description text DEFAULT ''::text
);


ALTER TABLE public.speciality OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 21327)
-- Name: speciality_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.speciality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.speciality_id_seq OWNER TO postgres;

--
-- TOC entry 3271 (class 0 OID 0)
-- Dependencies: 202
-- Name: speciality_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.speciality_id_seq OWNED BY public.speciality.id;


--
-- TOC entry 212 (class 1259 OID 21447)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id text NOT NULL,
    realm text,
    username text,
    email text NOT NULL,
    emailverified boolean,
    verificationtoken text
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 21456)
-- Name: usercredentials; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usercredentials (
    id text NOT NULL,
    password text NOT NULL,
    userid text NOT NULL
);


ALTER TABLE public.usercredentials OWNER TO postgres;

--
-- TOC entry 3101 (class 2604 OID 21405)
-- Name: appointment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment ALTER COLUMN id SET DEFAULT nextval('public.appointment_id_seq'::regclass);


--
-- TOC entry 3098 (class 2604 OID 21355)
-- Name: doctor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.doctor ALTER COLUMN id SET DEFAULT nextval('public.doctor_id_seq'::regclass);


--
-- TOC entry 3100 (class 2604 OID 21383)
-- Name: patient id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.patient ALTER COLUMN id SET DEFAULT nextval('public.patient_id_seq'::regclass);


--
-- TOC entry 3102 (class 2604 OID 21439)
-- Name: shift id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shift ALTER COLUMN id SET DEFAULT nextval('public.shift_id_seq'::regclass);


--
-- TOC entry 3096 (class 2604 OID 21332)
-- Name: speciality id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.speciality ALTER COLUMN id SET DEFAULT nextval('public.speciality_id_seq'::regclass);


--
-- TOC entry 3256 (class 0 OID 21402)
-- Dependencies: 209
-- Data for Name: appointment; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3252 (class 0 OID 21352)
-- Dependencies: 205
-- Data for Name: doctor; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3254 (class 0 OID 21380)
-- Dependencies: 207
-- Data for Name: patient; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3258 (class 0 OID 21436)
-- Dependencies: 211
-- Data for Name: shift; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3250 (class 0 OID 21329)
-- Dependencies: 203
-- Data for Name: speciality; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3259 (class 0 OID 21447)
-- Dependencies: 212
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3260 (class 0 OID 21456)
-- Dependencies: 213
-- Data for Name: usercredentials; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3272 (class 0 OID 0)
-- Dependencies: 208
-- Name: appointment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.appointment_id_seq', 1, false);


--
-- TOC entry 3273 (class 0 OID 0)
-- Dependencies: 204
-- Name: doctor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.doctor_id_seq', 1, false);


--
-- TOC entry 3274 (class 0 OID 0)
-- Dependencies: 206
-- Name: patient_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.patient_id_seq', 1, false);


--
-- TOC entry 3275 (class 0 OID 0)
-- Dependencies: 210
-- Name: shift_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.shift_id_seq', 1, false);


--
-- TOC entry 3276 (class 0 OID 0)
-- Dependencies: 202
-- Name: speciality_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.speciality_id_seq', 1, false);


--
-- TOC entry 3110 (class 2606 OID 21407)
-- Name: appointment appointment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment
    ADD CONSTRAINT appointment_pkey PRIMARY KEY (id);


--
-- TOC entry 3106 (class 2606 OID 21361)
-- Name: doctor doctor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.doctor
    ADD CONSTRAINT doctor_pkey PRIMARY KEY (id);


--
-- TOC entry 3108 (class 2606 OID 21388)
-- Name: patient patient_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.patient
    ADD CONSTRAINT patient_pkey PRIMARY KEY (id);


--
-- TOC entry 3112 (class 2606 OID 21441)
-- Name: shift shift_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shift
    ADD CONSTRAINT shift_pkey PRIMARY KEY (id);


--
-- TOC entry 3104 (class 2606 OID 21338)
-- Name: speciality speciality_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.speciality
    ADD CONSTRAINT speciality_pkey PRIMARY KEY (id);


--
-- TOC entry 3115 (class 2606 OID 21454)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 3117 (class 2606 OID 21463)
-- Name: usercredentials usercredentials_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usercredentials
    ADD CONSTRAINT usercredentials_pkey PRIMARY KEY (id);


--
-- TOC entry 3113 (class 1259 OID 21455)
-- Name: user_email_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX user_email_idx ON public."user" USING btree (email);


--
-- TOC entry 3120 (class 2606 OID 21413)
-- Name: appointment fk_appointment_doctorId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment
    ADD CONSTRAINT "fk_appointment_doctorId" FOREIGN KEY (doctorid) REFERENCES public.doctor(id);


--
-- TOC entry 3121 (class 2606 OID 21418)
-- Name: appointment fk_appointment_patientId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment
    ADD CONSTRAINT "fk_appointment_patientId" FOREIGN KEY (patientid) REFERENCES public.patient(id);


--
-- TOC entry 3119 (class 2606 OID 21408)
-- Name: appointment fk_appointment_specialityId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment
    ADD CONSTRAINT "fk_appointment_specialityId" FOREIGN KEY (specialityid) REFERENCES public.speciality(id);


--
-- TOC entry 3118 (class 2606 OID 21362)
-- Name: doctor fk_doctor_specialityId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.doctor
    ADD CONSTRAINT "fk_doctor_specialityId" FOREIGN KEY (specialityid) REFERENCES public.speciality(id);


--
-- TOC entry 3122 (class 2606 OID 21442)
-- Name: shift fk_shift_doctorId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shift
    ADD CONSTRAINT "fk_shift_doctorId" FOREIGN KEY (doctorid) REFERENCES public.doctor(id);


-- Completed on 2021-08-17 20:58:54 +03

--
-- PostgreSQL database dump complete
--

