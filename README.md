# doctor-appointment-system

A doctor appointment system in which users can create doctor and/or patient accounts. Doctors can manage their shifts, patients can make appointments with doctors with desired speciality.

Here are some of the basic actions you can do:

* **POST /signUp** : Sign up a new user. (Role must be given as `'doctor'` or `'patient'` under `realm` property in payload)
* **POST /login** : Login as an existing user
* **POST /doctor** : Create a doctor (max 1 per user)
* **POST /patient** : Create a patient (max 1 per user)
* **GET /doctors** : Search&filter doctors by firstName and specialityId
* **GET /shifts** : Get shifts available (doctors only)
* **POST /shifts** : Create a shift for a given time period
* **PATCH /shifts/\{id}** : Update shifts(doctors only, each doctor can modify only their shifts)
* **POST /appointments** : Create an appointment for a given speciality on a given date (doctor and exact time is given by the system)

The operations are not limited by the list above. For the entire list of endpoints please refer to [the Postman collection](assets/doctor-appointment-system.postman_collection.json).

# How to run

* Create an `.env` file right under the project folder. You can rename `env.dist` file to do that.

* Install Docker and docker-compose and then run:

    `docker-compose up --build`

* To run unit tests execute following command:

    `docker exec doctor-appointment-system_app_1 yarn test:unit`

# Schema diagram

![Schemas](assets/schemas.png?raw=true "Schema diagram")


[![LoopBack](https://github.com/strongloop/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png)](http://loopback.io/)
