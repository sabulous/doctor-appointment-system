import {belongsTo, Entity, model, property} from '@loopback/repository';
import {Doctor, DoctorWithRelations} from './doctor.model';

@model({
  settings: {
    strict: false,
    foreignKeys: {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      fk_shift_doctorId: {
        name: 'fk_shift_doctorId',
        entity: 'Doctor',
        entityKey: 'id',
        foreignKey: 'doctorid',
      },
    },
    hiddenProperties: ['doctorId'],
  },
})
export class Shift extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  userId?: string;

  @property({
    type: 'date',
    required: true,
  })
  start: string;

  @property({
    type: 'date',
    required: true,
  })
  end: string;

  @belongsTo(() => Doctor)
  doctorId: number;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Shift>) {
    super(data);
  }
}

export interface ShiftRelations {
  // describe navigational properties here
  doctor?: DoctorWithRelations;
}

export type ShiftWithRelations = Shift & ShiftRelations;
