export * from './doctor.model';
export * from './speciality.model';
export * from './appointment.model';
export * from './shift.model';
export * from './patient.model';
