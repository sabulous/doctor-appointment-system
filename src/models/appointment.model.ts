import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Doctor, DoctorWithRelations} from './doctor.model';
import {Patient, PatientWithRelations} from './patient.model';
import {Speciality} from './speciality.model';

@model({
  settings: {
    strict: false,
    foreignKeys: {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      fk_appointment_specialityId: {
        name: 'fk_appointment_specialityId',
        entity: 'Speciality',
        entityKey: 'id',
        foreignKey: 'specialityid',
      },
      // eslint-disable-next-line @typescript-eslint/naming-convention
      fk_appointment_doctorId: {
        name: 'fk_appointment_doctorId',
        entity: 'Doctor',
        entityKey: 'id',
        foreignKey: 'doctorid',
      },
      // eslint-disable-next-line @typescript-eslint/naming-convention
      fk_appointment_patientId: {
        name: 'fk_appointment_patientId',
        entity: 'Patient',
        entityKey: 'id',
        foreignKey: 'patientid',
      },
    },
    hiddenProperties: ['doctorId', 'patientId'],
  },
})
export class Appointment extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'date',
    required: true,
  })
  time: string;

  @property({
    type: 'number',
    required: true,
  })
  duration: number;

  @property({
    type: 'boolean',
    required: true,
  })
  isActive: boolean;

  @belongsTo(() => Speciality)
  specialityId: number;

  @belongsTo(() => Doctor)
  doctorId: number;

  @belongsTo(() => Patient)
  patientId: number;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Appointment>) {
    super(data);
  }
}

export interface AppointmentRelations {
  // describe navigational properties here
  doctor?: DoctorWithRelations;
  patient?: PatientWithRelations;
}

export type AppointmentWithRelations = Appointment & AppointmentRelations;
