import {
  belongsTo,
  Entity,
  model,
  property,
  hasMany,
} from '@loopback/repository';
import {Speciality, SpecialityWithRelations} from './speciality.model';
import {Shift} from './shift.model';

@model({
  settings: {
    strict: false,
    foreignKeys: {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      fk_doctor_specialityId: {
        name: 'fk_doctor_specialityId',
        entity: 'Speciality',
        entityKey: 'id',
        foreignKey: 'specialityid',
      },
    },
    hiddenProperties: ['specialityId'],
  },
})
export class Doctor extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  userId?: string;

  @property({
    type: 'string',
    required: true,
  })
  firstName: string;

  @property({
    type: 'string',
    required: true,
  })
  lastName: string;

  @property({
    type: 'string',
  })
  picture?: string;

  @property({
    type: 'string',
    default: '',
  })
  statement?: string;

  @belongsTo(() => Speciality)
  specialityId: number;

  @hasMany(() => Shift)
  shifts: Shift[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Doctor>) {
    super(data);
  }
}

export interface DoctorRelations {
  // describe navigational properties here
  speciality?: SpecialityWithRelations;
}

export type DoctorWithRelations = Doctor & DoctorRelations;
