import {DefaultCrudRepository} from '@loopback/repository';
import {Speciality, SpecialityRelations} from '../models';
import {PostgresDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class SpecialityRepository extends DefaultCrudRepository<
  Speciality,
  typeof Speciality.prototype.id,
  SpecialityRelations
> {
  constructor(@inject('datasources.postgres') dataSource: PostgresDataSource) {
    super(Speciality, dataSource);
  }
}
