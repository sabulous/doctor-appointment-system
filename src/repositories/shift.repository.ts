import {
  DefaultCrudRepository,
  repository,
  BelongsToAccessor,
} from '@loopback/repository';
import {Shift, ShiftRelations, Doctor} from '../models';
import {PostgresDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {DoctorRepository} from './doctor.repository';

export class ShiftRepository extends DefaultCrudRepository<
  Shift,
  typeof Shift.prototype.id,
  ShiftRelations
> {
  public readonly doctor: BelongsToAccessor<Doctor, typeof Shift.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('DoctorRepository')
    protected doctorRepositoryGetter: Getter<DoctorRepository>,
  ) {
    super(Shift, dataSource);
    this.doctor = this.createBelongsToAccessorFor(
      'doctor',
      doctorRepositoryGetter,
    );
    this.registerInclusionResolver('doctor', this.doctor.inclusionResolver);
  }
}
