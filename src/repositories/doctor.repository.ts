import {Getter, inject} from '@loopback/core';
import {
  BelongsToAccessor,
  DefaultCrudRepository,
  repository,
  HasManyRepositoryFactory,
} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {Doctor, DoctorRelations, Speciality, Shift} from '../models';
import {SpecialityRepository} from './speciality.repository';
import {ShiftRepository} from './shift.repository';

export class DoctorRepository extends DefaultCrudRepository<
  Doctor,
  typeof Doctor.prototype.id,
  DoctorRelations
> {
  public readonly speciality: BelongsToAccessor<
    Speciality,
    typeof Doctor.prototype.id
  >;

  public readonly shifts: HasManyRepositoryFactory<
    Shift,
    typeof Doctor.prototype.id
  >;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('SpecialityRepository')
    specialityRepositoryGetter: Getter<SpecialityRepository>,
    @repository.getter('ShiftRepository')
    protected shiftRepositoryGetter: Getter<ShiftRepository>,
  ) {
    super(Doctor, dataSource);

    this.shifts = this.createHasManyRepositoryFactoryFor(
      'shifts',
      shiftRepositoryGetter,
    );
    this.registerInclusionResolver('shifts', this.shifts.inclusionResolver);

    this.speciality = this.createBelongsToAccessorFor(
      'speciality',
      specialityRepositoryGetter,
    );
    this.registerInclusionResolver(
      'speciality',
      this.speciality.inclusionResolver,
    );
  }
}
