export * from './doctor.repository';
export * from './speciality.repository';
export * from './appointment.repository';
export * from './shift.repository';
export * from './patient.repository';
