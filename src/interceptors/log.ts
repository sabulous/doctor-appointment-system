import {Interceptor} from '@loopback/core';

const log: Interceptor = (invocationCtx, next) => {
  console.log(`LOG: Called: ${invocationCtx.targetName}`);
  const result = next();
  return result;
};

export default log;
