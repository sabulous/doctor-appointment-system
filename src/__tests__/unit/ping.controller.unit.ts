import {
  createStubInstance,
  expect,
  StubbedInstanceWithSinonAccessor,
} from '@loopback/testlab';
import {beforeEach} from 'mocha';
import {PingController} from '../../controllers';

describe('PingController (unit)', () => {
  let pingController: StubbedInstanceWithSinonAccessor<PingController>;

  beforeEach(givenStubbedRepository);

  describe('ping()', () => {
    it('should return a greeting property in response', async () => {
      // eslint-disable-next-line @typescript-eslint/await-thenable
      const res = await pingController.ping();
      expect(res).to.containEql({
        greeting: 'Hello from LoopBack',
      });
    });
  });

  function givenStubbedRepository() {
    pingController = createStubInstance(PingController);
    pingController.stubs.ping.resolves({
      greeting: 'Hello from LoopBack',
      date: new Date(),
      url: '',
      headers: {},
    });
  }
});
