import {UserProfile} from '@loopback/security';
import {
  createStubInstance,
  expect,
  StubbedInstanceWithSinonAccessor,
} from '@loopback/testlab';
import {beforeEach} from 'mocha';
import {DoctorController} from '../../controllers';

describe('DoctorController (unit)', () => {
  let doctorController: StubbedInstanceWithSinonAccessor<DoctorController>;
  let user: UserProfile;

  beforeEach(givenStubbedRepository);

  describe('create()', () => {
    it('should return success: true', async () => {
      const res = await doctorController.create(user, {
        firstName: 'docname',
        lastName: 'surname',
        picture: 'picURL',
        statement: 'culpa nulla sunt reprehenderit',
        specialityId: 1,
      });
      expect(res.success).to.eql(true);
    });

    it('should create a doctor instance with given details', async () => {
      const res = await doctorController.create(user, {
        firstName: 'docname',
        lastName: 'surname',
        picture: 'picURL',
        statement: 'culpa nulla sunt reprehenderit',
        specialityId: 1,
      });
      expect(res.data).to.containEql({
        firstName: 'docname',
        lastName: 'surname',
        picture: 'picURL',
        statement: 'culpa nulla sunt reprehenderit',
      });
    });
  });

  function givenStubbedRepository() {
    doctorController = createStubInstance(DoctorController);
    doctorController.stubs.create.resolves({
      success: true,
      data: {
        id: 2,
        userId: 'd740d349-04ed-4a20-bcfe-58f9c1344f75',
        firstName: 'docname',
        lastName: 'surname',
        picture: 'picURL',
        statement: 'culpa nulla sunt reprehenderit',
      },
    });
  }
});
