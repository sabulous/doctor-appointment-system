import {authenticate} from '@loopback/authentication';
import {inject, intercept, service} from '@loopback/core';
import {Count, CountSchema, repository, Where} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  requestBody,
  response,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import log from '../interceptors/log';
import {Patient} from '../models';
import {PatientRepository} from '../repositories';
import {PatientService} from '../services/patient.service';
import {GenericResponse} from '../services/types';

@authenticate('jwt')
@intercept(log)
export class PatientController {
  constructor(
    @repository(PatientRepository)
    public patientRepository: PatientRepository,
    @service(PatientService) public patientService: PatientService,
  ) {}

  @post('/patients')
  @response(200, {
    description: 'Patient model instance',
    content: {'application/json': {schema: getModelSchemaRef(Patient)}},
  })
  async create(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Patient, {
            title: 'NewPatient',
            exclude: ['id'],
          }),
        },
      },
    })
    patient: Omit<Patient, 'id'>,
  ): Promise<GenericResponse> {
    return this.patientService.create(patient, currentUserProfile.id);
  }

  @get('/patients/count')
  @response(200, {
    description: 'Patient model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Patient) where?: Where<Patient>): Promise<Count> {
    return this.patientRepository.count(where);
  }

  @get('/patients')
  @response(200, {
    description: 'Array of Patient model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Patient, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @param.query.string('firstName') firstName?: string,
  ): Promise<GenericResponse> {
    return this.patientService.getAll(currentUserProfile.id, firstName);
  }

  @patch('/patients')
  @response(200, {
    description: 'Patient PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Patient, {partial: true}),
        },
      },
    })
    patient: Patient,
    @param.where(Patient) where?: Where<Patient>,
  ): Promise<Count> {
    return this.patientRepository.updateAll(patient, where);
  }

  @get('/patients/{id}')
  @response(200, {
    description: 'Patient model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Patient, {includeRelations: true}),
      },
    },
  })
  async findById(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @param.path.number('id') id: number,
  ): Promise<GenericResponse> {
    return this.patientService.getById(id, currentUserProfile.id);
  }

  @patch('/patients/{id}')
  @response(204, {
    description: 'Patient PATCH success',
  })
  async updateById(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Patient, {partial: true}),
        },
      },
    })
    patient: Patient,
  ): Promise<GenericResponse> {
    return this.patientService.updateById(id, patient, currentUserProfile.id);
  }

  @del('/patients/{id}')
  @response(204, {
    description: 'Patient DELETE success',
  })
  async deleteById(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @param.path.number('id') id: number,
  ): Promise<GenericResponse> {
    return this.patientService.deleteById(id, currentUserProfile.id);
  }
}
