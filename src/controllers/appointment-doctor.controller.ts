import {authenticate} from '@loopback/authentication';
import {intercept} from '@loopback/core';
import {repository} from '@loopback/repository';
import {get, getModelSchemaRef, param} from '@loopback/rest';
import log from '../interceptors/log';
import {Appointment, Doctor} from '../models';
import {AppointmentRepository} from '../repositories';

@authenticate('jwt')
@intercept(log)
export class AppointmentDoctorController {
  constructor(
    @repository(AppointmentRepository)
    public appointmentRepository: AppointmentRepository,
  ) {}

  @get('/appointments/{id}/doctor', {
    responses: {
      '200': {
        description: 'Doctor belonging to Appointment',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Doctor)},
          },
        },
      },
    },
  })
  async getDoctor(
    @param.path.number('id') id: typeof Appointment.prototype.id,
  ): Promise<Doctor> {
    return this.appointmentRepository.doctor(id);
  }
}
