import {authenticate} from '@loopback/authentication';
import {inject, intercept, service} from '@loopback/core';
import {Count, CountSchema, repository, Where} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  requestBody,
  response,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import log from '../interceptors/log';
import {Doctor} from '../models';
import {DoctorRepository} from '../repositories';
import {DoctorService} from '../services';
import {GenericResponse} from '../services/types';

@authenticate('jwt')
@intercept(log)
export class DoctorController {
  constructor(
    @repository(DoctorRepository)
    public doctorRepository: DoctorRepository,
    @service(DoctorService) public doctorService: DoctorService, // @inject.getter('authentication.currentUser')
  ) {}

  @post('/doctors')
  @response(200, {
    description: 'Doctor model instance',
    content: {'application/json': {schema: getModelSchemaRef(Doctor)}},
  })
  async create(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Doctor, {
            title: 'NewDoctor',
            exclude: ['id'],
          }),
        },
      },
    })
    doctor: Omit<Doctor, 'id'>,
  ): Promise<GenericResponse> {
    return this.doctorService.create(doctor, currentUserProfile.id);
  }

  @get('/doctors/count')
  @response(200, {
    description: 'Doctor model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Doctor) where?: Where<Doctor>): Promise<Count> {
    return this.doctorRepository.count(where);
  }

  @get('/doctors')
  @response(200, {
    description: 'Array of Doctor model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Doctor, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.query.string('firstName') firstName?: string,
    @param.query.string('specialityId') specialityId?: string,
  ): Promise<Doctor[]> {
    return this.doctorService.getAll(firstName, specialityId);
  }

  @patch('/doctors')
  @response(200, {
    description: 'Doctor PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Doctor, {partial: true}),
        },
      },
    })
    doctor: Doctor,
    @param.where(Doctor) where?: Where<Doctor>,
  ): Promise<Count> {
    return this.doctorRepository.updateAll(doctor, where);
  }

  @get('/doctors/{id}')
  @response(200, {
    description: 'Doctor model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Doctor, {includeRelations: true}),
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Doctor> {
    return this.doctorService.getById(id);
  }

  @patch('/doctors/{id}')
  @response(204, {
    description: 'Doctor PATCH success',
  })
  async updateById(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Doctor, {partial: true}),
        },
      },
    })
    doctor: Doctor,
  ): Promise<GenericResponse> {
    return this.doctorService.updateById(id, doctor, currentUserProfile.id);
  }

  @del('/doctors/{id}')
  @response(204, {
    description: 'Doctor DELETE success',
  })
  async deleteById(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @param.path.number('id') id: number,
  ): Promise<GenericResponse> {
    return this.doctorService.deleteById(id, currentUserProfile.id);
  }
}
