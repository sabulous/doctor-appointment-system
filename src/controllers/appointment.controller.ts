import {authenticate} from '@loopback/authentication';
import {inject, intercept, service} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
  Response,
  RestBindings,
} from '@loopback/rest';
import log from '../interceptors/log';
import {Appointment} from '../models';
import {AppointmentRepository} from '../repositories';
import {AppointmentService} from '../services';
import {GenericResponse} from '../services/types';
import {HTTP} from './types';

@authenticate('jwt')
@intercept(log)
export class AppointmentController {
  constructor(
    @repository(AppointmentRepository)
    public appointmentRepository: AppointmentRepository,
    @service(AppointmentService)
    public appointmentService: AppointmentService,
    @inject(RestBindings.Http.RESPONSE) private res: Response,
  ) {}

  @post('/appointments')
  @response(200, {
    description: 'Appointment model instance',
    content: {'application/json': {schema: getModelSchemaRef(Appointment)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Appointment, {
            title: 'NewAppointment',
            exclude: ['id'],
            optional: ['doctorId'],
          }),
        },
      },
    })
    appointment: Omit<Appointment, 'id'>,
  ): Promise<GenericResponse> {
    const result = await this.appointmentService.create(appointment);
    this.res.status(result.success ? HTTP.OK : HTTP.UNPROCESSABLE_ENTITY);
    return result;
  }

  @get('/appointments/count')
  @response(200, {
    description: 'Appointment model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Appointment) where?: Where<Appointment>,
  ): Promise<Count> {
    return this.appointmentRepository.count(where);
  }

  @get('/appointments')
  @response(200, {
    description: 'Array of Appointment model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Appointment, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Appointment) filter?: Filter<Appointment>,
  ): Promise<Appointment[]> {
    return this.appointmentService.getAll(filter);
  }

  @patch('/appointments')
  @response(200, {
    description: 'Appointment PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Appointment, {partial: true}),
        },
      },
    })
    appointment: Appointment,
    @param.where(Appointment) where?: Where<Appointment>,
  ): Promise<Count> {
    return this.appointmentRepository.updateAll(appointment, where);
  }

  @get('/appointments/{id}')
  @response(200, {
    description: 'Appointment model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Appointment, {includeRelations: true}),
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Appointment> {
    return this.appointmentService.getById(id);
  }

  @patch('/appointments/{id}')
  @response(204, {
    description: 'Appointment PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Appointment, {partial: true}),
        },
      },
    })
    appointment: Appointment,
  ): Promise<void> {
    await this.appointmentRepository.updateById(id, appointment);
  }

  @put('/appointments/{id}')
  @response(204, {
    description: 'Appointment PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() appointment: Appointment,
  ): Promise<void> {
    await this.appointmentRepository.replaceById(id, appointment);
  }

  @del('/appointments/{id}')
  @response(204, {
    description: 'Appointment DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.appointmentRepository.deleteById(id);
  }
}
