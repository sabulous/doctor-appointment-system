import {authenticate} from '@loopback/authentication';
import {intercept} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import log from '../interceptors/log';
import {Doctor, Shift} from '../models';
import {DoctorRepository} from '../repositories';

@authenticate('jwt')
@intercept(log)
export class DoctorShiftController {
  constructor(
    @repository(DoctorRepository) protected doctorRepository: DoctorRepository,
  ) {}

  @get('/doctors/{id}/shifts', {
    responses: {
      '200': {
        description: 'Array of Doctor has many Shift',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Shift)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Shift>,
  ): Promise<Shift[]> {
    return this.doctorRepository.shifts(id).find(filter);
  }

  @post('/doctors/{id}/shifts', {
    responses: {
      '200': {
        description: 'Doctor model instance',
        content: {'application/json': {schema: getModelSchemaRef(Shift)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Doctor.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Shift, {
            title: 'NewShiftInDoctor',
            exclude: ['id', 'doctorId'],
          }),
        },
      },
    })
    shift: Omit<Shift, 'id'>,
  ): Promise<Shift> {
    return this.doctorRepository.shifts(id).create(shift);
  }

  @patch('/doctors/{id}/shifts', {
    responses: {
      '200': {
        description: 'Doctor.Shift PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Shift, {partial: true}),
        },
      },
    })
    shift: Partial<Shift>,
    @param.query.object('where', getWhereSchemaFor(Shift)) where?: Where<Shift>,
  ): Promise<Count> {
    return this.doctorRepository.shifts(id).patch(shift, where);
  }

  @del('/doctors/{id}/shifts', {
    responses: {
      '200': {
        description: 'Doctor.Shift DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Shift)) where?: Where<Shift>,
  ): Promise<Count> {
    return this.doctorRepository.shifts(id).delete(where);
  }
}
