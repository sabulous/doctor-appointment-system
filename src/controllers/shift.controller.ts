import {authenticate} from '@loopback/authentication';
import {inject, intercept, service} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  requestBody,
  response,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import log from '../interceptors/log';
import {Shift} from '../models';
import {ShiftRepository} from '../repositories';
import {ShiftService} from '../services';
import {GenericResponse} from '../services/types';

@authenticate('jwt')
@intercept(log)
export class ShiftController {
  constructor(
    @repository(ShiftRepository)
    public shiftRepository: ShiftRepository,
    @service(ShiftService)
    public shiftService: ShiftService,
  ) {}

  @post('/shifts')
  @response(200, {
    description: 'Shift model instance',
    content: {'application/json': {schema: getModelSchemaRef(Shift)}},
  })
  async create(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Shift, {
            title: 'NewShift',
            exclude: ['id'],
          }),
        },
      },
    })
    shift: Omit<Shift, 'id'>,
  ): Promise<GenericResponse> {
    return this.shiftService.create(shift, currentUserProfile.id);
  }

  @get('/shifts')
  @response(200, {
    description: 'Array of Shift model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Shift, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @param.filter(Shift) filter?: Filter<Shift>,
  ): Promise<GenericResponse> {
    return this.shiftService.getAll(currentUserProfile.id, filter);
  }

  @patch('/shifts')
  @response(200, {
    description: 'Shift PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Shift, {partial: true}),
        },
      },
    })
    shift: Shift,
    @param.where(Shift) where?: Where<Shift>,
  ): Promise<Count> {
    return this.shiftRepository.updateAll(shift, where);
  }

  @get('/shifts/{id}')
  @response(200, {
    description: 'Shift model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Shift, {includeRelations: true}),
      },
    },
  })
  async findById(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @param.path.number('id') id: number,
  ): Promise<GenericResponse> {
    return this.shiftService.getById(currentUserProfile.id, id);
  }

  @patch('/shifts/{id}')
  @response(204, {
    description: 'Shift PATCH success',
  })
  async updateById(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Shift, {partial: true}),
        },
      },
    })
    shift: Shift,
  ): Promise<GenericResponse> {
    return this.shiftService.updateById(id, shift, currentUserProfile.id);
  }

  @del('/shifts/{id}')
  @response(204, {
    description: 'Shift DELETE success',
  })
  async deleteById(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @param.path.number('id') id: number,
  ): Promise<GenericResponse> {
    return this.shiftService.deleteById(id, currentUserProfile.id);
  }
}
