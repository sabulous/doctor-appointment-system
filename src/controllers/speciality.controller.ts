import {authenticate} from '@loopback/authentication';
import {intercept, service} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import log from '../interceptors/log';
import {Speciality} from '../models';
import {SpecialityRepository} from '../repositories';
import {SpecialityService} from '../services';

@authenticate('jwt')
@intercept(log)
export class SpecialityController {
  constructor(
    @repository(SpecialityRepository)
    public specialityRepository: SpecialityRepository,
    @service(SpecialityService) public specialityService: SpecialityService,
  ) {}

  @post('/specialities')
  @response(200, {
    description: 'Speciality model instance',
    content: {'application/json': {schema: getModelSchemaRef(Speciality)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Speciality, {
            title: 'NewSpeciality',
            exclude: ['id'],
          }),
        },
      },
    })
    speciality: Omit<Speciality, 'id'>,
  ): Promise<Speciality> {
    return this.specialityRepository.create(speciality);
  }

  @get('/specialities/count')
  @response(200, {
    description: 'Speciality model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Speciality) where?: Where<Speciality>,
  ): Promise<Count> {
    return this.specialityRepository.count(where);
  }

  @get('/specialities')
  @response(200, {
    description: 'Array of Speciality model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Speciality, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Speciality) filter?: Filter<Speciality>,
  ): Promise<Speciality[]> {
    return this.specialityService.getAll(filter);
  }

  @patch('/specialities')
  @response(200, {
    description: 'Speciality PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Speciality, {partial: true}),
        },
      },
    })
    speciality: Speciality,
    @param.where(Speciality) where?: Where<Speciality>,
  ): Promise<Count> {
    return this.specialityRepository.updateAll(speciality, where);
  }

  @get('/specialities/{id}')
  @response(200, {
    description: 'Speciality model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Speciality, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Speciality, {exclude: 'where'})
    filter?: FilterExcludingWhere<Speciality>,
  ): Promise<Speciality> {
    return this.specialityRepository.findById(id, filter);
  }

  @patch('/specialities/{id}')
  @response(204, {
    description: 'Speciality PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Speciality, {partial: true}),
        },
      },
    })
    speciality: Speciality,
  ): Promise<void> {
    await this.specialityRepository.updateById(id, speciality);
  }

  @put('/specialities/{id}')
  @response(204, {
    description: 'Speciality PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() speciality: Speciality,
  ): Promise<void> {
    await this.specialityRepository.replaceById(id, speciality);
  }

  @del('/specialities/{id}')
  @response(204, {
    description: 'Speciality DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.specialityRepository.deleteById(id);
  }
}
