import {User, UserRepository} from '@loopback/authentication-jwt';
import {BindingScope, injectable} from '@loopback/core';
import {repository} from '@loopback/repository';
import {Doctor} from '../models';
import {DoctorRepository} from '../repositories';
import {GenericResponse} from './types';

@injectable({scope: BindingScope.TRANSIENT})
export class DoctorService {
  constructor(
    @repository(DoctorRepository)
    public doctorRepository: DoctorRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {}

  public getAll(firstName?: string, specialityId?: string): Promise<Doctor[]> {
    const filter = {
      where: {
        firstName: firstName ? {like: `%${firstName}%`} : undefined,
        specialityId,
      },
      include: ['speciality', 'shifts'],
    };
    return this.doctorRepository.find(filter);
  }

  public getById(id: number): Promise<Doctor> {
    const filter = {include: ['speciality', 'shifts']};
    return this.doctorRepository.findById(id, filter);
  }

  public async create(
    doctorWithoutId: Omit<Doctor, 'id'>,
    userId: string,
  ): Promise<GenericResponse> {
    try {
      const isAuthorized = await this.authorize(
        userId,
        ['doctor', 'admin'],
        'Only users with realm "doctor" can create a doctor profile.',
      );
      if (!isAuthorized.success) {
        return isAuthorized;
      }
      const user = isAuthorized.data as User;

      if (user && user.realm !== 'admin') {
        const prevRecord = await this.doctorRepository.findOne({
          where: {userId},
        });
        if (prevRecord) {
          return {
            success: false,
            message: 'A user cannot create more than one doctor.',
          };
        }
      }

      const doctor = Object.assign(doctorWithoutId, {userId});
      const result = await this.doctorRepository.create(doctor);
      return {success: true, data: result};
    } catch (e) {
      return {success: false, message: e.message};
    }
  }

  public async updateById(
    id: number,
    doctor: Partial<Doctor>,
    userId: string,
  ): Promise<GenericResponse> {
    try {
      const isAuthorized = await this.authorize(
        userId,
        ['doctor', 'admin'],
        'Only users with realm "doctor" can update a doctor profile.',
        true,
        id,
      );
      if (!isAuthorized.success) {
        return isAuthorized;
      }

      await this.doctorRepository.updateById(id, doctor);
      return {success: true};
    } catch (e) {
      return {success: false, message: e.message};
    }
  }

  public async deleteById(
    id: number,
    userId: string,
  ): Promise<GenericResponse> {
    try {
      const isAuthorized = await this.authorize(
        userId,
        ['doctor', 'admin'],
        'Only users with realm "doctor" can delete a doctor profile.',
        true,
        id,
      );
      if (!isAuthorized.success) {
        return isAuthorized;
      }

      await this.doctorRepository.deleteById(id);
      return {success: true, data: {id}};
    } catch (e) {
      return {success: false, message: e.message};
    }
  }

  private async authorize(
    userId: string,
    realms: string[],
    errorMessage: string,
    requireOwnership?: boolean,
    targetId?: number,
  ): Promise<GenericResponse> {
    const user = await this.userRepository.findById(userId);
    if (!user || !user.id || !user.realm) {
      return {success: false, message: 'User not found.'};
    }
    if (!realms.includes(user.realm)) {
      return {
        success: false,
        message: errorMessage,
      };
    }
    if (requireOwnership) {
      const record = await this.doctorRepository.findOne({where: {userId}});
      if (record && record.id !== targetId) {
        return {
          success: false,
          message:
            'You have to be the owner of the entity to complete this action.',
        };
      }
    }
    return {success: true, data: user};
  }
}
