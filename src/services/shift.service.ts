import {UserRepository} from '@loopback/authentication-jwt';
import {BindingScope, injectable} from '@loopback/core';
import {Filter, repository} from '@loopback/repository';
import {Shift} from '../models';
import {DoctorRepository, ShiftRepository} from '../repositories';
import {GenericResponse, ShiftWithSpeciality} from './types';

@injectable({scope: BindingScope.TRANSIENT})
export class ShiftService {
  constructor(
    @repository(ShiftRepository)
    public shiftRepository: ShiftRepository,
    @repository(DoctorRepository)
    public doctorRepository: DoctorRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {}

  public async getAll(
    userId: string,
    filter?: Filter<Shift>,
  ): Promise<GenericResponse> {
    try {
      const result = await this.shiftRepository.find(
        Object.assign(filter ?? {}, {where: {userId}, include: ['doctor']}),
      );
      return {success: true, data: result};
    } catch (e) {
      return {success: false, message: e.message};
    }
  }

  public async getById(userId: string, id: number): Promise<GenericResponse> {
    try {
      const filter = {where: {userId}, include: ['doctor']};
      const result = await this.shiftRepository.findById(id, filter);
      return {success: true, data: result};
    } catch (e) {
      return {success: false, message: e.message};
    }
  }

  public async create(
    shiftWithoutId: Omit<Shift, 'id'>,
    userId: string,
  ): Promise<GenericResponse> {
    try {
      const user = await this.userRepository.findById(userId);
      const doctor = await this.doctorRepository.findOne({
        where: {userId: user.id},
      });
      if (!doctor) {
        return {
          success: false,
          message: 'Doctor associated with the user not found.',
        };
      }
      if (doctor.id !== shiftWithoutId.doctorId) {
        return {
          success: false,
          message: 'A doctor cannot create a shift for another doctor.',
        };
      }

      const isAuthorized = await this.authorize(
        userId,
        ['doctor', 'admin'],
        'Only users with realm "doctor" can create a shift.',
      );
      if (!isAuthorized.success) {
        return isAuthorized;
      }

      const shift = Object.assign(shiftWithoutId, {userId});
      const result = await this.shiftRepository.create(shift);
      return {success: true, data: result};
    } catch (e) {
      return {success: false, message: e.message};
    }
  }

  public async updateById(
    id: number,
    shift: Partial<Shift>,
    userId: string,
  ): Promise<GenericResponse> {
    try {
      const user = await this.userRepository.findById(userId);
      const doctor = await this.doctorRepository.findOne({
        where: {userId: user.id},
      });
      if (!doctor) {
        return {
          success: false,
          message: 'Doctor associated with the user not found.',
        };
      }
      if (doctor.id !== shift.doctorId) {
        return {
          success: false,
          message: 'A doctor cannot assign a shift to another doctor.',
        };
      }

      const isAuthorized = await this.authorize(
        userId,
        ['doctor', 'admin'],
        'Only users with realm "doctor" can update a shift.',
      );
      if (!isAuthorized.success) {
        return isAuthorized;
      }

      await this.shiftRepository.updateById(id, shift);
      return {success: true};
    } catch (e) {
      return {success: false, message: e.message};
    }
  }

  public async deleteById(
    id: number,
    userId: string,
  ): Promise<GenericResponse> {
    try {
      const user = await this.userRepository.findById(userId);
      const doctor = await this.doctorRepository.findOne({
        where: {userId: user.id},
      });
      if (!doctor) {
        return {
          success: false,
          message: 'Doctor associated with the user not found.',
        };
      }
      const shift = await this.shiftRepository.findById(id);
      if (doctor.id !== shift.doctorId) {
        return {
          success: false,
          message: "A doctor cannot delete another doctor's shift.",
        };
      }

      const isAuthorized = await this.authorize(
        userId,
        ['doctor', 'admin'],
        'Only users with realm "doctor" can delete a shift.',
      );
      if (!isAuthorized.success) {
        return isAuthorized;
      }

      await this.shiftRepository.deleteById(id);
      return {success: true};
    } catch (e) {
      return {success: false, message: e.message};
    }
  }

  public async getMatchingShifts(
    date: string,
    specId: number,
  ): Promise<ShiftWithSpeciality[]> {
    const filter: Filter = {
      include: [
        {
          relation: 'shifts',
          scope: {
            where: {
              and: [
                {start: {lt: new Date(date).setHours(23, 59, 59, 999)}},
                {end: {gt: new Date(date).setHours(0, 0, 0, 0)}},
              ],
            },
          },
        },
      ],
      where: {
        specialityId: specId,
      },
    };

    const doctors = await this.doctorRepository.find(filter);

    return doctors
      .filter(doctor => !!doctor.shifts?.length)
      .map(doctor => doctor.shifts)
      .reduce((d1, d2) => [...d1, ...d2], [])
      .map(shift => ({specialityId: specId, ...shift}));
  }

  private async authorize(
    userId: string,
    realms: string[],
    errorMessage: string,
  ): Promise<GenericResponse> {
    const user = await this.userRepository.findById(userId);
    if (!user || !user.id || !user.realm) {
      return {success: false, message: 'User not found.'};
    }
    if (!realms.includes(user.realm)) {
      return {
        success: false,
        message: errorMessage,
      };
    }
    return {success: true, data: user};
  }
}
