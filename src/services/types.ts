import {Appointment} from '../models/appointment.model';

export type ShiftWithSpeciality = {
  id?: number;
  start: string;
  end: string;
  doctorId: number;
  specialityId: number;
};

export type ShiftWithAppointments = {
  shift: ShiftWithSpeciality;
  appointments: Appointment[];
};

export type GenericResponse = {
  success: boolean;
  data?: object;
  message?: string;
};
