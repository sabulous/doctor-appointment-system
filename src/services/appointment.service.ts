import {BindingScope, injectable, service} from '@loopback/core';
import {Filter, repository} from '@loopback/repository';
import {Appointment} from '../models';
import {AppointmentRepository} from '../repositories';
import {ShiftService} from './shift.service';
import {
  GenericResponse,
  ShiftWithAppointments,
  ShiftWithSpeciality,
} from './types';

@injectable({scope: BindingScope.TRANSIENT})
export class AppointmentService {
  constructor(
    @repository(AppointmentRepository)
    public appointmentRepository: AppointmentRepository,
    @service(ShiftService)
    public shiftService: ShiftService,
  ) {}

  public getAll(filter?: Filter<Appointment>): Promise<Appointment[]> {
    return this.appointmentRepository.find(
      Object.assign(filter ?? {}, {include: ['doctor', 'patient']}),
    );
  }

  public getById(id: number): Promise<Appointment> {
    const filter = {include: ['doctor', 'patient']};
    return this.appointmentRepository.findById(id, filter);
  }

  // get appointments of a speciality that falls between date range [start, end)
  public async getMatchingAppointments(
    start: string,
    end: string,
    doctorId: number,
    specialityId: number,
  ) {
    const filter: Filter = {
      where: {
        and: [
          {time: {gte: new Date(start)}},
          {time: {lt: new Date(end)}},
          {isActive: true},
          {specialityId},
          {doctorId},
        ],
      },
    };

    const appointments = await this.getAll(filter);
    return appointments.sort(
      (a, b) => new Date(a.time).getTime() - new Date(b.time).getTime(),
    );
  }

  public async getShiftsWithAppointments(
    requestedTime: string,
    specialityId: number,
  ): Promise<ShiftWithAppointments[]> {
    const shifts: ShiftWithSpeciality[] =
      await this.shiftService.getMatchingShifts(requestedTime, specialityId);

    const shiftsWithAppointments = await Promise.all(
      shifts
        .sort(
          (a, b) => new Date(a.start).getTime() - new Date(b.start).getTime(),
        )
        .map(async shift => ({
          shift,
          appointments: await this.getMatchingAppointments(
            shift.start,
            shift.end,
            shift.doctorId,
            specialityId,
          ),
        })),
    );

    return shiftsWithAppointments;
  }

  public checkAvailability(
    shiftsWithAppointments: ShiftWithAppointments[],
    date: string,
    duration: number,
  ): [Date, number] | null {
    let startTime = 0;
    let doctorId = 0;
    let availabilityFound = false;
    for (const shiftWithAppointment of shiftsWithAppointments) {
      doctorId = shiftWithAppointment.shift.doctorId;

      startTime = Math.max(
        new Date(date).setHours(0, 0, 0, 0),
        new Date(shiftWithAppointment.shift.start).getTime(),
      );

      for (const currentAppointment of shiftWithAppointment.appointments) {
        let availableDuration;
        // if not the same day, skip
        if (
          new Date(currentAppointment.time).setHours(0, 0, 0, 0) ===
          new Date(startTime).setHours(0, 0, 0, 0)
        ) {
          availableDuration =
            new Date(currentAppointment.time).getTime() -
            new Date(startTime).getTime();
        }

        if (availableDuration && availableDuration >= duration * 60 * 1000) {
          availabilityFound = true;
          break;
        } else {
          startTime = new Date(
            new Date(currentAppointment.time).getTime() +
              currentAppointment.duration * 60 * 1000,
          ).getTime();
        }
      }

      const lastAvailablePeriod =
        new Date(shiftWithAppointment.shift.end).getTime() -
        new Date(startTime).getTime();
      if (lastAvailablePeriod >= duration * 60 * 1000) {
        availabilityFound = true;
      }

      if (availabilityFound) {
        break;
      }
    }

    return availabilityFound ? [new Date(startTime), doctorId] : null;
  }

  public async create(
    appointment: Omit<Appointment, 'id'>,
  ): Promise<GenericResponse> {
    if (appointment.duration < 15) {
      return {
        success: false,
        message: 'Appointment duration must be at least 15 minutes.',
      };
    }

    const shiftsWithAppointments = await this.getShiftsWithAppointments(
      appointment.time,
      appointment.specialityId,
    );

    const availability = this.checkAvailability(
      shiftsWithAppointments,
      appointment.time,
      appointment.duration,
    );

    if (availability) {
      const [startTime, doctorId] = availability;
      const data = {
        ...appointment,
        time: startTime.toString(),
        isActive: true,
        doctorId,
      };

      await this.appointmentRepository.create(data);

      return {
        success: true,
        data,
      };
    } else {
      return {success: false, message: 'Availability not found'};
    }
  }
}
