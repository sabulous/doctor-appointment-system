import {BindingScope, injectable} from '@loopback/core';
import {Filter, repository} from '@loopback/repository';
import {Speciality} from '../models';
import {SpecialityRepository} from '../repositories';

@injectable({scope: BindingScope.TRANSIENT})
export class SpecialityService {
  constructor(
    @repository(SpecialityRepository)
    public specialityRepository: SpecialityRepository,
  ) {}

  public getAll(filter?: Filter<Speciality>): Promise<Speciality[]> {
    return this.specialityRepository.find(filter);
  }
}
