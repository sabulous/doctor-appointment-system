import {User, UserRepository} from '@loopback/authentication-jwt';
import {BindingScope, injectable} from '@loopback/core';
import {repository} from '@loopback/repository';
import {Patient} from '../models';
import {PatientRepository} from '../repositories';
import {GenericResponse} from './types';

@injectable({scope: BindingScope.TRANSIENT})
export class PatientService {
  constructor(
    @repository(PatientRepository)
    public patientRepository: PatientRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {}

  public async getAll(
    userId: string,
    firstName?: string,
  ): Promise<GenericResponse> {
    try {
      const isAuthorized = await this.authorize(
        userId,
        ['doctor', 'admin'],
        'Only users with realm "doctor" or "admin" can create a patient profile.',
      );
      if (!isAuthorized.success) {
        return isAuthorized;
      }

      const filter = {
        where: {
          firstName: firstName ? {like: `%${firstName}%`} : undefined,
        },
      };
      const result = await this.patientRepository.find(filter);
      return {success: true, data: result};
    } catch (e) {
      return {success: false, message: e.message};
    }
  }

  public async getById(id: number, userId: string): Promise<GenericResponse> {
    try {
      const isAuthorized = await this.authorize(
        userId,
        ['patient', 'doctor', 'admin'],
        'Only users with realm "doctor" or "admin" can create a patient profile.',
        true,
        id,
        ['doctor', 'admin'],
      );
      if (!isAuthorized.success) {
        return isAuthorized;
      }
      const result = await this.patientRepository.findById(id);
      return {success: true, data: result};
    } catch (e) {
      return {success: false, message: e.message};
    }
  }

  public async create(
    patientWithoutId: Omit<Patient, 'id'>,
    userId: string,
  ): Promise<GenericResponse> {
    try {
      const isAuthorized = await this.authorize(
        userId,
        ['patient', 'doctor', 'admin'],
        'Only users with realm "patient" or "doctor" can create a patient profile.',
      );
      if (!isAuthorized.success) {
        return isAuthorized;
      }
      const user = isAuthorized.data as User;

      // prevent users not an admin from creating more than one patient
      if (user && user.realm !== 'admin') {
        const prevRecord = await this.patientRepository.findOne({
          where: {userId},
        });
        if (prevRecord) {
          return {
            success: false,
            message: 'A user cannot create more than one patient.',
          };
        }
      }

      const patient = Object.assign(patientWithoutId, {userId});
      const result = await this.patientRepository.create(patient);
      return {success: true, data: result};
    } catch (e) {
      return {success: false, message: e.message};
    }
  }

  public async updateById(
    id: number,
    patient: Partial<Patient>,
    userId: string,
  ): Promise<GenericResponse> {
    try {
      const isAuthorized = await this.authorize(
        userId,
        ['patient', 'doctor', 'admin'],
        'Only users with realm "patient" or "doctor" can update a patient profile.',
        true,
        id,
      );
      if (!isAuthorized.success) {
        return isAuthorized;
      }

      await this.patientRepository.updateById(id, patient);
      return {success: true};
    } catch (e) {
      return {success: false, message: e.message};
    }
  }

  public async deleteById(
    id: number,
    userId: string,
  ): Promise<GenericResponse> {
    try {
      const isAuthorized = await this.authorize(
        userId,
        ['patient', 'admin'],
        'Only users with realm "patient" can delete a patient profile.',
        true,
        id,
        ['admin'],
      );
      if (!isAuthorized.success) {
        return isAuthorized;
      }

      await this.patientRepository.deleteById(id);
      return {success: true, data: {id}};
    } catch (e) {
      return {success: false, message: e.message};
    }
  }

  private async authorize(
    userId: string,
    realms: string[],
    errorMessage: string,
    requireOwnership?: boolean,
    targetId?: number,
    fullAccess?: string[],
  ): Promise<GenericResponse> {
    const user = await this.userRepository.findById(userId);
    if (!user || !user.id || !user.realm) {
      return {success: false, message: 'User not found.'};
    }
    if (!realms.includes(user.realm)) {
      return {
        success: false,
        message: errorMessage,
      };
    }
    if (!fullAccess?.includes(user.realm) && requireOwnership) {
      const record = await this.patientRepository.findOne({where: {userId}});

      if (record && record.id !== targetId) {
        return {
          success: false,
          message:
            'You have to be the owner of the entity to complete this action.',
        };
      }
    }
    return {success: true, data: user};
  }
}
